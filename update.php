<?php
include_once 'dbConnection.php';
session_start();
$email = $_SESSION['email'];

// Fungsi untuk menangani kesalahan
function handle_error($message) {
    error_log($message);
    die($message);
}

// delete feedback
if (isset($_SESSION['key']) && isset($_GET['fdid']) && $_SESSION['key'] == 'sunny7785068889') {
    $id = $_GET['fdid'];
    if (!mysqli_query($con, "DELETE FROM feedback WHERE id='$id'")) {
        handle_error('Error deleting feedback: ' . mysqli_error($con));
    }
    header("location:dash.php?q=3");
    exit();
}

// delete user
if (isset($_SESSION['key']) && isset($_GET['demail']) && $_SESSION['key'] == 'sunny7785068889') {
    $demail = $_GET['demail'];
    if (!mysqli_query($con, "DELETE FROM `rank` WHERE email='$demail'")) {
        handle_error('Error deleting rank: ' . mysqli_error($con));
    }
    if (!mysqli_query($con, "DELETE FROM history WHERE email='$demail'")) {
        handle_error('Error deleting history: ' . mysqli_error($con));
    }
    if (!mysqli_query($con, "DELETE FROM user WHERE email='$demail'")) {
        handle_error('Error deleting user: ' . mysqli_error($con));
    }
    header("location:dash.php?q=1");
    exit();
}

// remove quiz
if (isset($_SESSION['key']) && isset($_GET['q']) && $_GET['q'] == 'rmquiz' && $_SESSION['key'] == 'sunny7785068889') {
    $eid = $_GET['eid'];
    $result = mysqli_query($con, "SELECT * FROM questions WHERE eid='$eid'");
    if (!$result) {
        handle_error('Error selecting questions: ' . mysqli_error($con));
    }
    while ($row = mysqli_fetch_array($result)) {
        $qid = $row['qid'];
        if (!mysqli_query($con, "DELETE FROM options WHERE qid='$qid'")) {
            handle_error('Error deleting options: ' . mysqli_error($con));
        }
        if (!mysqli_query($con, "DELETE FROM answer WHERE qid='$qid'")) {
            handle_error('Error deleting answer: ' . mysqli_error($con));
        }
    }
    if (!mysqli_query($con, "DELETE FROM questions WHERE eid='$eid'")) {
        handle_error('Error deleting questions: ' . mysqli_error($con));
    }
    if (!mysqli_query($con, "DELETE FROM quiz WHERE eid='$eid'")) {
        handle_error('Error deleting quiz: ' . mysqli_error($con));
    }
    if (!mysqli_query($con, "DELETE FROM history WHERE eid='$eid'")) {
        handle_error('Error deleting history: ' . mysqli_error($con));
    }
    header("location:dash.php?q=5");
    exit();
}

// add quiz
if (isset($_SESSION['key']) && isset($_GET['q']) && $_GET['q'] == 'addquiz' && $_SESSION['key'] == 'sunny7785068889') {
    $name = ucwords(strtolower($_POST['name']));
    $total = $_POST['total'];
    $sahi = $_POST['right'];
    $wrong = $_POST['wrong'];
    $time = $_POST['time'];
    $tag = $_POST['tag'];
    $desc = $_POST['desc'];
    $id = uniqid();
    if (!mysqli_query($con, "INSERT INTO quiz VALUES ('$id', '$name', '$sahi', '$wrong', '$total', '$time', '$desc', '$tag', NOW())")) {
        handle_error('Error adding quiz: ' . mysqli_error($con));
    }
    header("location:dash.php?q=4&step=2&eid=$id&n=$total");
    exit();
}

// add question
if (isset($_SESSION['key']) && isset($_GET['q']) && $_GET['q'] == 'addqns' && $_SESSION['key'] == 'sunny7785068889') {
    $n = $_GET['n'];
    $eid = $_GET['eid'];
    $ch = $_GET['ch'];

    for ($i = 1; $i <= $n; $i++) {
        $qid = uniqid();
        $qns = $_POST['qns' . $i];
        if (!mysqli_query($con, "INSERT INTO questions VALUES ('$eid', '$qid', '$qns', '$ch', '$i')")) {
            handle_error('Error adding question: ' . mysqli_error($con));
        }
        $oaid = uniqid();
        $obid = uniqid();
        $ocid = uniqid();
        $odid = uniqid();
        $a = $_POST[$i . '1'];
        $b = $_POST[$i . '2'];
        $c = $_POST[$i . '3'];
        $d = $_POST[$i . '4'];
        if (!mysqli_query($con, "INSERT INTO options VALUES ('$qid', '$a', '$oaid')")) {
            handle_error('Error adding option a: ' . mysqli_error($con));
        }
        if (!mysqli_query($con, "INSERT INTO options VALUES ('$qid', '$b', '$obid')")) {
            handle_error('Error adding option b: ' . mysqli_error($con));
        }
        if (!mysqli_query($con, "INSERT INTO options VALUES ('$qid', '$c', '$ocid')")) {
            handle_error('Error adding option c: ' . mysqli_error($con));
        }
        if (!mysqli_query($con, "INSERT INTO options VALUES ('$qid', '$d', '$odid')")) {
            handle_error('Error adding option d: ' . mysqli_error($con));
        }
        $e = $_POST['ans' . $i];
        switch ($e) {
            case 'a':
                $ansid = $oaid;
                break;
            case 'b':
                $ansid = $obid;
                break;
            case 'c':
                $ansid = $ocid;
                break;
            case 'd':
                $ansid = $odid;
                break;
            default:
                $ansid = $oaid;
        }
        if (!mysqli_query($con, "INSERT INTO answer VALUES ('$qid', '$ansid')")) {
            handle_error('Error adding answer: ' . mysqli_error($con));
        }
    }
    header("location:dash.php?q=0");
    exit();
}
  
// quiz start
if (isset($_GET['q']) && $_GET['q'] == 'quiz' && isset($_GET['step']) && $_GET['step'] == 2) {
    $eid = $_GET['eid'];
    $sn = $_GET['n'];
    $total = $_GET['t'];
    $ans = $_POST['ans'] ?? null;
    $qid = $_GET['qid'];
    $query = "SELECT * FROM answer WHERE qid='$qid'";
$result = mysqli_query($con, $query);
if (!$result) {
    handle_error('Error fetching answer: ' . mysqli_error($con));
}
$row = mysqli_fetch_array($result);
$ansid = $row['ansid'];

if ($ans == $ansid) {
    $query = "SELECT * FROM quiz WHERE eid='$eid'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching quiz: ' . mysqli_error($con));
    }
    $row = mysqli_fetch_array($result);
    $sahi = $row['sahi'];

    if ($sn == 1) {
        $query = "INSERT INTO history (email, eid, score, level, sahi, wrong, date) VALUES ('$email', '$eid', 0, 0, 0, 0, NOW())";
        $result = mysqli_query($con, $query);
        if (!$result) {
            handle_error('Error initializing history: ' . mysqli_error($con));
        }
    }

    $query = "SELECT * FROM history WHERE eid='$eid' AND email='$email'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching history: ' . mysqli_error($con));
    }
    $row = mysqli_fetch_array($result);
    $s = $row['score'];
    $r = $row['sahi'];
    $r++;
    $s += $sahi;

    $query = "UPDATE history SET score=$s, level=$sn, sahi=$r, date=NOW() WHERE email='$email' AND eid='$eid'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error updating history: ' . mysqli_error($con));
    }
} else {
    $query = "SELECT * FROM quiz WHERE eid='$eid'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching quiz: ' . mysqli_error($con));
    }
    $row = mysqli_fetch_array($result);
    $wrong = $row['wrong'];

    if ($sn == 1) {
        $query = "INSERT INTO history (email, eid, score, level, sahi, wrong, date) VALUES ('$email', '$eid', 0, 0, 0, 0, NOW())";
        $result = mysqli_query($con, $query);
        if (!$result) {
            handle_error('Error initializing history: ' . mysqli_error($con));
        }
    }

    $query = "SELECT * FROM history WHERE eid='$eid' AND email='$email'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching history: ' . mysqli_error($con));
    }
    $row = mysqli_fetch_array($result);
    $s = $row['score'];
    $w = $row['wrong'];
    $w++;
    $s -= $wrong;

    $query = "UPDATE history SET score=$s, level=$sn, wrong=$w, date=NOW() WHERE email='$email' AND eid='$eid'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error updating history: ' . mysqli_error($con));
    }
}

if ($sn != $total) {
    $sn++;
    header("location:account.php?q=quiz&step=2&eid=$eid&n=$sn&t=$total");
} else if ($_SESSION['key'] != 'sunny7785068889') {
    $query = "SELECT score FROM history WHERE eid='$eid' AND email='$email'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching score: ' . mysqli_error($con));
    }
    $row = mysqli_fetch_array($result);
    $s = $row['score'];

    $query = "SELECT * FROM `rank` WHERE email='$email'";
    $result = mysqli_query($con, $query);
    if (!$result) {
        handle_error('Error fetching rank: ' . mysqli_error($con));
    }
    $rowcount = mysqli_num_rows($result);
    if ($rowcount == 0) {
        $query = "INSERT INTO `rank` (email, score, time) VALUES ('$email', '$s', NOW())";
        $result = mysqli_query($con, $query);
        if (!$result) {
            handle_error('Error inserting rank: ' . mysqli_error($con));
        }
    } else {
        $row = mysqli_fetch_array($result);
        $sun = $row['score'];
        $sun += $s;

        $query = "UPDATE `rank` SET score=$sun, time=NOW() WHERE email='$email'";
        $result = mysqli_query($con, $query);
        if (!$result) {
            handle_error('Error updating rank: ' . mysqli_error($con));
        }
    }
    header("location:account.php?q=result&eid=$eid");
} else {
    header("location:account.php?q=result&eid=$eid");
}
exit();
}

// restart quiz
if (isset($_GET['q']) && $_GET['q'] == 'quizre' && isset($_GET['step']) && $_GET['step'] == 25) {
$eid = $_GET['eid'];
$n = $_GET['n'];
$t = $_GET['t'];
$query = "SELECT score FROM history WHERE eid='$eid' AND email='$email'";
$result = mysqli_query($con, $query);
if (!$result) {
    handle_error('Error fetching score: ' . mysqli_error($con));
}
$row = mysqli_fetch_array($result);
$s = $row['score'];

$query = "DELETE FROM `history` WHERE eid='$eid' AND email='$email'";
$result = mysqli_query($con, $query);
if (!$result) {
    handle_error('Error deleting history: ' . mysqli_error($con));
}

$query = "SELECT * FROM `rank` WHERE email='$email'";
$result = mysqli_query($con, $query);
if (!$result) {
    handle_error('Error fetching rank: ' . mysqli_error($con));
}
$row = mysqli_fetch_array($result);
$sun = $row['score'];
$sun -= $s;

$query = "UPDATE `rank` SET score=$sun, time=NOW() WHERE email='$email'";
$result = mysqli_query($con, $query);
if (!$result) {
    handle_error('Error updating rank: ' . mysqli_error($con));
}
header("location:account.php?q=quiz&step=2&eid=$eid&n=1&t=$t");
exit();
}

// Pastikan tidak ada output sebelum header
ob_start();
ob_end_flush();
?>
